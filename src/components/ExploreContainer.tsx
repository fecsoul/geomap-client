import React, { useEffect, useState } from 'react';
import { BackgroundGeolocation } from '@ionic-native/background-geolocation';

interface ExploreContainerProps {
  name: string;
}

const ExploreContainer: React.FC<ExploreContainerProps> = ({ name }) => {
  const [latestLocation, setLatestLocation] = useState<any | null>(null); // Cambié a 'any' por ahora

  useEffect(() => {
    const startBackgroundGeolocation = () => {
      const config = {
        desiredAccuracy: 10,
        stationaryRadius: 20,
        distanceFilter: 30,
        debug: true,
        stopOnTerminate: false,
        interval: 2000,
      };

      BackgroundGeolocation.configure(config).then(() => {
        BackgroundGeolocation.start();

        BackgroundGeolocation.getLocations().then((locations: any[]) => {
          // Actualiza el estado con la última ubicación
          setLatestLocation(locations[locations.length - 1]);
        });
      });
    };

    startBackgroundGeolocation();

    return () => {
      BackgroundGeolocation.stop();
    };
  }, []); // Se ejecuta solo una vez al montar el componente

  return (
    <div className="container">
      <strong>{name}</strong>
      <p>On serve <a target="_blank" rel="noopener noreferrer" href="https://ionicframework.com/docs/components">UI Components</a></p>
      {latestLocation && (
        <p>
          Última Ubicación: Latitud {latestLocation.latitude}, Longitud {latestLocation.longitude}
        </p>
      )}
    </div>
  );
};

export default ExploreContainer;
